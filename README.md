# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this Assignment for? ###

* As Full stack developer , you want to show case your skills
  

### What is given? ###
 data/vegetables.xls- grocery price data
* Item Name - Vegetable/fruit/grocery name
* Date- date
* Price - price of the item on that date

### What is expected out of you ###

Rest End Point

1.Generate a list of vegetable/fruit sorted by name, their maximum price ,date on which price was max

2.Generate a report for each Item Name, showing how their price trended

 

GUI

-Design UI to show the data [rest output] based on Item Name



### Special communication ###
    - Provide the instruction on how to run the application
    - Front end framework options are React / Angular/ Vuejs/ LitHtml/ Polymer [any one]
    - Backend will be a spring boot rest application.
    - TDD is mandatory.
    - Feel free to communicate if some requirements or expectations are not clear.
    - If you make any assumptions then state them when you submit the assignment
    - Special enhancement attract extra points and state or give snapshot of the enhancement when you submit.

 